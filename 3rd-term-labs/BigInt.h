#include <iostream>
#include <string>
#ifndef BIGINT_H_INCLUDED
#define BIGINT_H_INCLUDED
class BigInt
{
private:
	static char DIGIT_AMOUNT;
	static char MAX_NUMBER_PART_LENGTH;
	static char MAX_NUMBER_LENGTH;
	static int BASE;

	int sign; //sign of a number. -1 for negative, 1 for positive and zero
	int *digits; //a1*base + a0
	void copy(int *inputDigits, int inputSign) {
		sign = inputSign;
		digits = new int[DIGIT_AMOUNT];
		for (int i = 0; i <= DIGIT_AMOUNT; i++)
			digits[i] = inputDigits[i];
	}
	std::string toStringUnsigned();
	int* getCopyOfDigits();
	int* BigInt::sumDigits(int *lhs, int *rhs);
	int* greaterSubstractSmaller(int *lhs, int *rhs);
	int* multiplyBySmaller(int *lhs, int *rhs);
public:
	BigInt(); //default constructor
	BigInt(std::string number);//string constructor
	BigInt(int * digits, int sign);
	BigInt(BigInt &obj); //copy constructor
	~BigInt();// destructor

	static int compare(BigInt &lhs, BigInt &rhs);
	int getSign();
	void setSign(int aSign);
	int *getDigits();
	void setDigits(int *aDigits);
	std::string toString();
	int BigInt::length();

	/* operator overloading */
	friend std::ostream& operator<<(std::ostream&, BigInt&);
	BigInt& operator=(const BigInt& op);
	BigInt operator+(BigInt op2);

	/* the relational operators */
	inline bool operator==(const BigInt& rhs) { return compareTo(rhs) == 0; }
	inline bool operator!=(const BigInt& rhs) { return compareTo(rhs) != 0; }
	inline bool operator< (const BigInt& rhs) { return compareTo(rhs) <  0; }
	inline bool operator> (const BigInt& rhs) { return compareTo(rhs) >  0; }
	inline bool operator<=(const BigInt& rhs) { return compareTo(rhs) <= 0; }
	inline bool operator>=(const BigInt& rhs) { return compareTo(rhs) >= 0; }

	/*operations */

	/**
		Compares this BigInt with the specified BigInt.
		@param the specified object
		@return a negative integer (-1) , zero, or a positive integer (+1)
		as the current object is less than, equal to, or greater than the second.
	*/
	int compareTo(const BigInt &obj);

	/**
		Compares abs of this BigInt with the abs of specified BigInt.
		@param the specified object
		@return a negative integer (-1) , zero, or a positive integer (+1)
		as the current object is less than, equal to, or greater than the second.
	*/
	int absCompareTo(BigInt &obj);

	/**
		Returns a BigInt whose value is (this + val).
	*/
	void add(BigInt &val);

	/**
	Returns a BigInt whose value is (this - val).
	*/
	void substract(BigInt &val);

	/**
		Multipies this BigInt by value (this * val).
	*/
	void muliply(BigInt &val);
};


#endif // BIGINT_H_INCLUDED
