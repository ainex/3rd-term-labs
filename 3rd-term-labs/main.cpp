#include <iostream>
#include "BigInt.h"
#include "BigIntTest.h"
#include "ListTest.h"
using namespace std;

int main()
{
	//cout << "BigInt class demo" << endl;
	//runBigIntTestSuite();

	cout << "list<BigInt> class demo" << endl;
	runListTestSuite();

	return 0;
}