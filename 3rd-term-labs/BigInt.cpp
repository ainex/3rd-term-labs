#include "BigInt.h"
using namespace std;
char BigInt::DIGIT_AMOUNT = 2;
char BigInt::MAX_NUMBER_PART_LENGTH = 9;
char BigInt::MAX_NUMBER_LENGTH = MAX_NUMBER_PART_LENGTH *DIGIT_AMOUNT;
int BigInt::BASE = 1000 * 1000 * 1000;

BigInt::BigInt()
{
	sign = 0;
	digits = new int[DIGIT_AMOUNT];
	for (int i = 0; i < DIGIT_AMOUNT; i++)
	{
		digits[i] = 0;
	};
	cout << "constructor(), new object " << this << endl;

};

BigInt::BigInt(string signedNumber)
{

	cout << "constructor(" << signedNumber << ")" << endl;
	string number;
	//check sign
	if (signedNumber.front() == '-')
	{
		number = signedNumber.substr(1, signedNumber.size() - 1);
		sign = -1;
	}
	else
	{
		number = signedNumber;
		sign = 1;
	}

	if (number.size() > MAX_NUMBER_LENGTH)
	{
		throw invalid_argument("to BigInt: invalid string");
	}

	digits = new int[DIGIT_AMOUNT];

	if (number.size() > MAX_NUMBER_PART_LENGTH)
	{
		//really big integer number
		string position1 = number.substr(0, number.size() - MAX_NUMBER_PART_LENGTH);
		string position0 = number.substr(number.size() - MAX_NUMBER_PART_LENGTH, MAX_NUMBER_PART_LENGTH);

		digits[0] = stoi(position0);
		digits[1] = stoi(position1);

	}
	else
	{
		digits[0] = stoi(number);
		digits[1] = 0;
	}

	cout << "new object " << this << " , value : " << *this << endl;
}

BigInt::BigInt(int *inputDigits, int inputSign = 1)
{
	digits = new int[DIGIT_AMOUNT];
	for (int i = 0; i < DIGIT_AMOUNT; i++)
	{
		digits[i] = inputDigits[i];
	};
	sign = inputSign;
	cout << "constructor({" << inputDigits[0] << "," << inputDigits[1] << "}, sign=" << inputSign << "), new object: " << this << " , value : " << *this << endl;
};

BigInt::BigInt(BigInt &obj) {
	copy(obj.digits, obj.sign);
	cout << "copy " << &obj << " to " << this << endl;
}

BigInt::~BigInt()
{
	delete[]digits;
	cout << "destructor of the object " << this << endl;
};

int BigInt::compare(BigInt &lhs, BigInt &rhs)
{
	for (int i = DIGIT_AMOUNT - 1; i >= 0; i--)
	{
		if (lhs.sign * lhs.digits[i] > rhs.sign * rhs.digits[i]) return 1;
		if (lhs.sign * lhs.digits[i] < rhs.sign * rhs.digits[i]) return -1;
	}
	return 0;
}

int BigInt::getSign()
{
	return sign;
}

void BigInt::setSign(int aSign)
{
	if (aSign >= 0)
	{
		sign = 1;
	}
	else
	{
		sign = -1;
	}
}

int* BigInt::getDigits()
{
	return digits;
};

void BigInt::setDigits(int *aDigits)
{
	delete[]digits;
	digits = aDigits;
}

string BigInt::toString()
{
	string signSymbol = "";
	if (sign == -1)
	{
		signSymbol = "-";
	}

	return signSymbol + toStringUnsigned();
}

int BigInt::length()
{
	string unsignedString = toStringUnsigned();

	return (int)unsignedString.size();
}

std::ostream &operator<<(std::ostream &os, BigInt &obj)
{
	return os << obj.toString();
}

BigInt& BigInt::operator=(const BigInt &op)
{
	if (&op == this) return *this; //this is the same object
	delete[]digits;                    
	copy(op.digits, op.sign);              
	return *this; 
}                 


BigInt BigInt::operator+(BigInt op2)
{
	op2.add(*this);
	return op2;
}

//operatons


int BigInt::compareTo(const BigInt &obj)
{
	for (int i = DIGIT_AMOUNT - 1; i >= 0; i--)
	{
		if (sign * digits[i] > obj.sign * obj.digits[i]) return 1;
		if (sign * digits[i] < obj.sign * obj.digits[i]) return -1;
	}
	return 0;
}

int BigInt::absCompareTo(BigInt &obj)
{
	for (int i = DIGIT_AMOUNT - 1; i >= 0; i--)
	{
		if (digits[i] > obj.digits[i]) return 1;
		if (digits[i] < obj.digits[i]) return -1;
	}
	return 0;
}

void BigInt::add(BigInt &val)
{
	if (sign == val.sign) {
		int *result = sumDigits(getDigits(), val.getDigits());
		setDigits(result);
	}
	else
		//-A + B or A + (-B)
		if (1 == absCompareTo(val)) // |A| > |B| 
		{
			int *lhs = getDigits();
			int *rhs = val.getDigits();
			int *result = greaterSubstractSmaller(lhs, rhs);
			
			setDigits(result);
			if (sign == -1) //case -A + B <=> -1 (A - B)
			{ setSign(-1); }
		}
		else // |A| <= |B|
		{
			int *lhs = val.getDigits();
			int *rhs = getDigits();
			int *result = greaterSubstractSmaller(lhs, rhs);
			
			setDigits(result);
			//define sign
			if (sign == -1) //- A + B, lhs is less than rhs <=> signum(B - A) == 1
			{ 
				setSign(1); 
			}
			else //A + (-B), lhs is less than rhs <=> signum(A - B) == -1
			{ 
				setSign(-1); 
			}
		};


}

void BigInt::substract(BigInt &val)
{
	if (sign != val.sign) { // -A - (+B) or A - (-B) => signum(A) and |A| + |B|
		int *result = sumDigits(getDigits(), val.getDigits());
		setDigits(result);
		return;
	}

	//A - B or -A - (-B)
	if (1 == absCompareTo(val)) // |A| > |B| 
	{
		int *lhs = getDigits();
		int *rhs = val.getDigits();
		int *result = greaterSubstractSmaller(lhs, rhs);

		setDigits(result);
		
	}
	else // |A| <= |B|
	{
		int *lhs = val.getDigits();
		int *rhs = getDigits();
		int *result = greaterSubstractSmaller(lhs, rhs);

		setDigits(result);
		//define sign
		if (sign == -1) //-A - (-B), -A+B, lhs is less than rhs <=> signum(B - A) == 1
		{
			setSign(1);
		}
		else //A - B, lhs is less than rhs <=> signum(A - B) == -1
		{
			setSign(-1);
		}
	};



}

void BigInt::muliply(BigInt &val)
{
	int thisLength = length();
	int valLength = val.length();
	if (valLength + thisLength > MAX_NUMBER_LENGTH)
	{
		throw invalid_argument("multiply BigInt: multiplication result is too long");
	}

	int *result;
	if (valLength <= MAX_NUMBER_PART_LENGTH)
		result = multiplyBySmaller(getDigits(), val.getDigits());
	else
		result = multiplyBySmaller(val.getDigits(), getDigits());

	setSign(sign * val.getSign());
	setDigits(result);

}


//private 
string BigInt::toStringUnsigned()
{
	string result;
	if (digits[1] != 0)
	{
		string yangers = to_string(digits[0]);
		result = to_string(digits[1]) + yangers.insert(0, MAX_NUMBER_PART_LENGTH - yangers.size(), '0');
	}
	else
	{
		result = to_string(digits[0]);
	}

	return result;
}

int* BigInt::getCopyOfDigits()
{
	int *copy = new int[DIGIT_AMOUNT];
	memcpy(digits, copy, sizeof(int)*DIGIT_AMOUNT);
	return copy;
}

int* BigInt::sumDigits(int *lhs, int *rhs)
{
	int *result = new int[DIGIT_AMOUNT];
	int overflow = 0;
	for (int i = 0; i < DIGIT_AMOUNT; i++)
	{
		int currentResult = (lhs[i] + rhs[i]) + overflow;
		overflow = currentResult / BASE;
		result[i] = currentResult % BASE;
	}
	return result;
}

int* BigInt::greaterSubstractSmaller(int *lhs, int *rhs)
{
	int *result = new int[DIGIT_AMOUNT];
	int borrowing = 0;
	for (int i = 0; i < DIGIT_AMOUNT; i++)
	{
		int currentResult = (lhs[i] - rhs[i]) - borrowing;
		if (currentResult < 0) {
			currentResult += BASE; borrowing = 1;
		}
		else { borrowing = 0; }
		result[i] = currentResult;
	}
	return result;
}

int *BigInt::multiplyBySmaller(int *lhs, int *rhs)
{
	long long lshNumber = 1ll * lhs[1] * BASE + lhs[0];
	long long resultNumber = lshNumber * rhs[0];
	int *result = new int[DIGIT_AMOUNT];
	result[1] = resultNumber / BASE;
	result[0] = resultNumber%BASE;
	return result;
}