#pragma once
#include <iostream>
#include <string>
#include "BigInt.h"
using namespace std;

template <class T> class list {
	list<T> *next;
	T data;
	list(T& v) { data = v; next = NULL; }

public:   list() { next = NULL; }
		  ~list() {
			  if (next != NULL) delete next;
		  }

		  void addBegining(T& v) {
			  list *q = new list(v);
			  q->next = next;
			  next = q;
		  }
		  void addEnd(T &v) {
			  list *q = new list(v);
			  list *p;
			  //���� ����� ������
			  for (p = this; p->next != NULL; p = p->next);
			  //��������� ����� ������� � ����� 
			  p->next = q;
		  }

		  void insertKeepOrder(T& newdata)
		  {
			  list<T> *p, *q;
			  //����� ������� ������
			  p = new list<T>(newdata);
			  //�������� �� ������ � ���������� ������� �� ���������
			  for (q = this; q->next != NULL && newdata > q->next->data; q = q->next);

			  //����� �������, ��������� ����� ��������
			  p->next = q->next;
			  q->next = p;
		  }

		  // ���������� �� ������, ��������� � 0
		  T remove(int n)
		  {
			  list<T> *q, *p; //�������������� ��������� �� ���������� �������                            
			  for (q = this; q->next != NULL && n != 0; n--, q = q->next);
			  T val;
			  if (q->next == NULL) return NULL;
			  val = q->next->data;
			  p = q->next;
			  // ��������� ������� �� �������
			  q->next = p->next;
			  p->next = NULL;
			  delete p;
			  return val;
		  }
		  //���������� � ���� int, ��������� ������ ������
		  operator int() {
			  list *q; int n;
			  for (q = next, n = 0; q != NULL; n++, q = q->next);
			  return n;
		  }

		  friend std::ostream &operator<<(std::ostream& o, list<T>& obj)
		  {
			  list<T> *q;

			  //������ ������
			  o << (int)obj << endl;
			  //��������
			  for (q = obj.next; q != NULL; q = q->next) o << q->data << endl;
			  return o;
		  }

};

