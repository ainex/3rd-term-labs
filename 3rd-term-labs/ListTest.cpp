#include "ListTest.h"

using namespace std;

void testAddBegining()
{
	cout << "\nRUNNING testAddBegining()" << endl;
	list<BigInt> bigIntegers;

	for (int i = 20; i < 25; i++)
	{
		BigInt element(std::to_string(i));
		bigIntegers.addBegining(element);
	}
	cout << "list<BigInt>" << endl;
	cout << bigIntegers << endl;
}

void testInsertKeepOrder()
{
	cout << "\nRUNNING testInsertKeepOrder()" << endl;
	list<BigInt> bigIntegers;
	for (int i = 10; i < 15; i++)
	{
		BigInt element(std::to_string(i));
		bigIntegers.addEnd(element);
	}
	cout << "list<BigInt>" << endl;
	cout << bigIntegers << endl;

	BigInt element("12");
	cout << "add new element: " << element << endl;

	bigIntegers.insertKeepOrder(element);
	cout << "list<BigInt>" << endl;
	cout << bigIntegers << endl;
}

void testRemove()
{
	cout << "\nRUNNING testRemove()" << endl;
	list<BigInt> bigIntegers;
	for (int i = 11; i <= 15; i++)
	{
		BigInt element(std::to_string(i));
		bigIntegers.addEnd(element);
	}
	cout << "list<BigInt>" << endl;
	cout << bigIntegers << endl;

	cout << "remove element with index[3]" << endl;
	bigIntegers.remove(3);
	cout << "list<BigInt>" << endl;
	cout << bigIntegers << endl;

	cout << "remove element with index[0]" << endl;

	bigIntegers.remove(0);
	cout << "list<BigInt>" << endl;
	cout << bigIntegers << endl;
}

void runListTestSuite()
{
	testAddBegining();
	testInsertKeepOrder();
	testRemove();
}