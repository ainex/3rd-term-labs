#include "BigIntTest.h"
using namespace std;

void runBigIntTestSuite()
{
	cout << decorate("TESTS") << endl;
	cout << "starting BigInt test suite" << endl;

	testConstructorFromString();
	testCopyConstructor();
	testInternalConstructor();
	testReturnInternals();
	testOperatorOverloadToCout();
	testLength();

	/** OPERATIONS **/
	testCompareTo();
	testAdd();
	testSubstract();
	testMultiply();

	testOperatorOverloading();

	cout << decorate("TESTS PASSED SUCCESSFULLY") << endl;
}

void testConstructorFromString()
{
	cout << "\nRUNNING testConstructorFromString" << endl;

	try
	{
		BigInt numberFormOutOfRangeString("1234567891234567890");
	}
	catch (std::exception &e)
	{
		std::cout << "expected exception encounterd: [" << e.what() << "]" << std::endl;
	}

	BigInt numberLongString("12123456789");

	BigInt numberString("123456789");

	BigInt negativeNumberString("-123456789");

}

void testCopyConstructor()
{
	cout << "\nRUNNING testCopyConstructor" << endl;
	BigInt number("-125");
	BigInt copiedNumber(number);
	assert(number.getSign() == copiedNumber.getSign());
	assert(number.getDigits()[0] == copiedNumber.getDigits()[0]);
	assert(number.getDigits()[1] == copiedNumber.getDigits()[1]);

}

void testInternalConstructor()
{
	cout << "\nRUNNING testInternalConstructor" << endl;
	int digits[] = { 123456789, 123 };
	BigInt number(digits, -1);

	cout << "internal constructed obj: " << number << endl;

}

void testReturnInternals()
{
	cout << "\nRUNNING testReturnInternals" << endl;
	BigInt number("12345678912");
	int * digits = number.getDigits();
	assert(digits[1] == 12);
	assert(digits[0] == 345678912);
	cout << "got pointer to internals: " << digits << " contaning : " << digits[1] << digits[0] << endl;
}

void testOperatorOverloadToCout()
{
	cout << "\nRUNNING testOperatorOverloadToCout" << endl;
	BigInt number("-12345678912");
	cout << "operator << overload :" << number << endl;

	BigInt numberShort("-678912");
	cout << "operator << overload :" << numberShort << endl;

	BigInt numberPositiveShort("8912");
	cout << "operator << overload :" << numberPositiveShort << endl;

	BigInt numberZero("0");
	cout << "operator << overload :" << numberZero << endl;

	BigInt numberLong("123000056789");
	cout << "operator << overload :" << numberLong << endl;
}

void testLength()
{
	cout << "\nRUNNING testLength" << endl;
	BigInt number("-123");
	assert(3 == number.length());

	BigInt numberZero;
	assert(1 == numberZero.length());

	BigInt numberLong("123456789123456789");
	assert(18 == numberLong.length());

}

void testCompareTo()
{
	cout << "\nRUNNING testCompareTo" << endl;
	BigInt sample("123000456789");
	BigInt negative("-123000456789");
	assert(1 == sample.compareTo(negative));

	BigInt equal("123000456789");
	assert(0 == sample.compareTo(equal));

	BigInt zero;
	assert(1 == sample.compareTo(zero));

	BigInt bigger1("555000456789");
	assert(-1 == sample.compareTo(bigger1));

	BigInt bigger2("123000456799");
	assert(-1 == sample.compareTo(bigger2));

	assert(-1 == BigInt::compare(sample,bigger1));
}

void testAdd()
{
	cout << "\nRUNNING testAdd" << endl;
	BigInt lhs1("123000456789");
	BigInt rhs1("123000999999");
	BigInt expected1("246001456788");
	lhs1.add(rhs1);
	assert(0 == lhs1.compareTo(expected1));

	BigInt lhs2("-123000456789");
	BigInt rhs2("999999");
	BigInt expected2("-122999456790");
	lhs2.add(rhs2);
	assert(0 == lhs2.compareTo(expected2));

	BigInt lhs3;
	BigInt rhs3;
	BigInt expected3("0");
	lhs3.add(rhs3);
	assert(0 == lhs3.compareTo(expected3));

	BigInt lhs4("-25");
	BigInt rhs4("50");
	BigInt expected4("25");
	lhs4.add(rhs4);
	assert(0 == lhs4.compareTo(expected4));

	BigInt lhs5("0");
	BigInt rhs5("-50");
	BigInt expected5("-50");
	lhs5.add(rhs5);
	assert(0 == lhs5.compareTo(expected5));

	BigInt lhs6("1999999999");
	BigInt rhs6("-2999999999");
	BigInt expected6("-1000000000");
	lhs6.add(rhs6);
	assert(0 == lhs6.compareTo(expected6));

	BigInt lhs7("123456");
	BigInt rhs7("-10");
	BigInt expected7("123446");
	lhs7.add(rhs7);
	assert(0 == lhs7.compareTo(expected7));
}

void testSubstract()
{
	cout << "\nRUNNING testSubstract" << endl;

	BigInt lhs1("123000456789");
	BigInt rhs1("-123000999999");
	BigInt expected1("246001456788");
	lhs1.substract(rhs1);
	assert(0 == lhs1.compareTo(expected1));

	BigInt lhs2("-123000456789");
	BigInt rhs2("999999");
	BigInt expected2("-123001456788");
	lhs2.substract(rhs2);
	assert(0 == lhs2.compareTo(expected2));

	BigInt lhs3;
	BigInt rhs3;
	BigInt expected3("0");
	lhs3.substract(rhs3);
	assert(0 == lhs3.compareTo(expected3));

	
	BigInt lhs4("25");
	BigInt rhs4("50");
	BigInt expected4("-25");
	lhs4.substract(rhs4);
	assert(0 == lhs4.compareTo(expected4));
	
	BigInt lhs5("0");
	BigInt rhs5("-50");
	BigInt expected5("50");
	lhs5.substract(rhs5);
	assert(0 == lhs5.compareTo(expected5));

	BigInt lhs6("-1999999999");
	BigInt rhs6("-2999999999");
	BigInt expected6("1000000000");
	lhs6.substract(rhs6);
	assert(0 == lhs6.compareTo(expected6));

}

void testMultiply()
{
	cout << "\nRUNNING testMultiply" << endl;
	BigInt lhs1("-123456789");
	BigInt rhs1("123456789");
	BigInt expected1("-15241578750190521");
	lhs1.muliply(rhs1);
	assert(0 == lhs1.compareTo(expected1));

	BigInt lhs2("-2");
	BigInt rhs2("123456789123456");
	BigInt expected2("-246913578246912");
	lhs2.muliply(rhs2);
	assert(0 == lhs2.compareTo(expected2));

	BigInt lhs3("1234567891");
	BigInt rhs3("123456789");
	try
	{
		lhs3.muliply(rhs3);
	}
	catch (std::exception &e)
	{
		std::cout << "expected exception encounterd: [" << e.what() << "]" << std::endl;
	}

}

void testOperatorOverloading()
{
	cout << "\nRUNNING testOperatorOverloading" << endl;
	BigInt lhs1("123456");
	BigInt rhs1("-10");
	BigInt expected1("123446");
	BigInt result1 = lhs1 + rhs1;
	assert(0 == expected1.compareTo(result1));

	BigInt lhs2("123123456789");
	BigInt rhs2("123123456789");
	assert(true == (lhs2 == rhs2));
	assert(false == (lhs2 != rhs2));
	assert(true == (lhs2 >= rhs2));
	assert(true == (lhs2 <= rhs2));

	BigInt lhs3("123123456789");
	BigInt rhs3("-123123456789");
	assert(true == (lhs3 > rhs3));
	assert(false == (lhs3 < rhs3));
	assert(true == (rhs3 < lhs3));
	
}

std::string decorate(std::string str)
{
	return "-------------------------------------------------------\n" + str + "\n-------------------------------------------------------\n";
}

